# Rocket Science Automator README #

### What is this repository for? ###

* Science!

### How do I get set up? ###

* Install test app: http://www.serialporttool.com/RelayTimerR2XInfo.htm
* Install Ruby (must be 2.0.0-p481): http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.0.0-p481.exe?direct
* Install Gems
	* gem install ocra
	* gem install uia
	* gem install sinatra
	* gem install psych
	* gem install auto_click
* To build, run build.bat (Ctrl+C once server starts to end test run)
* Open Relay Timer app
* Run bin\rocket-science-automator.exe

###  Useful Development Tools ###

* Windows SDK: http://msdn.microsoft.com/en-us/windows/desktop/bg162891.aspx
	* Inspect http://msdn.microsoft.com/en-us/library/windows/desktop/dd318521(v=vs.85).aspx

### Gem Docs ###

* https://github.com/larsch/ocra
* https://github.com/northwoodspd/uia
* http://www.sinatrarb.com/intro.html