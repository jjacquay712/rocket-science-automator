#include <GUIConstantsEx.au3>
$gui = GUICreate("Simple Test",900)
GUICtrlCreateLabel("This will run stuff", 30, 10)
;Create buttons
$start = GuiCtrlCreateButton("Start", 10, 40, 200, 100)
$start2 = GuiCtrlCreateButton("Something",300,40, 200,100)
$what = GuiCtrlCreateButton("What???",600,40, 200,100)
$kill = GuiCtrlCreateButton("Kill",300,200, 200,100)

;Create a large input box
GUICtrlCreateLabel("Seconds:", 30, 10)
GUISetFont (20); set font size to 20
$input1=GUICtrlCreateInput("",10,300,100,50,0x2000) ;Create input box, enforce numbers only

;Start gui
GUISetState(@SW_SHOW,$gui)

$relayhandle = 0

;GUI event handler
While 1
   $status = GUIGetMsg()
   Switch $status ;read status of GUI, compare with different button values to see if one is pressed
	  Case $start ;Start button
		 Local $value = GUICtrlRead($input1) ;get value from input box
		 $relayhandle = RelayTimerR2X(0,$value)  ;call RelayTimerR2X with 0 (default example) and input value
	  Case $start2
		 $maxhandle = MaxDl(0,0)

	  Case $kill ;Kill button
		 WinKill($relayhandle)
		 WinKill($maxhandle)
		 ExitLoop


	  Case $GUI_EVENT_CLOSE
		 ExitLoop
   EndSwitch
WEnd


;Function for RelayTimerR2X
Func RelayTimerR2X($md,$arg1)
   If $md = 0 Then ;mode of operation 0 (automatic)
		 Local $MyPid= Run("C:\Program Files (x86)\Serial Port Tool\Relay Timer R2X\RelayTimer.exe") ;Run program

		 ;Deal with nagware
		 $nag=WinWaitActive("Register ","",5)
		 if $nag <> 0 Then
			WinWaitActive("Register","Register Later button will be enabled after 0 Second",10)
			ControlClick($nag,"","Register &Later")
		 EndIf

		 ;process input value, if null, use 30 as default
		 if $arg1 = "" Then
			Local $value = 30
		 Else
			Local $value = $arg1
		 EndIf

		 ;Run RelayTimerR2X, click on some controls
		 Local $winid = WinWaitActive("Relay Timer R2X")
		 ControlClick($winid,"","WindowsForms10.Window.8.app.0.378734a2") ;set all to off for consistency
		 ControlClick($winid,"","WindowsForms10.Window.8.app.0.378734a17")
		 ControlClick($winid,"","WindowsForms10.Window.8.app.0.378734a14")
		 ControlClick($winid,"","WindowsForms10.BUTTON.app.0.378734a1")
		 ControlClick($winid,"","WindowsForms10.Window.8.app.0.378734a15") ;Call override for top
		 $winoverride = WinWaitActive("Override Manual")
		 ControlSetText($winoverride,"","WindowsForms10.EDIT.app.0.378734a1",$value) ;set value for time
		 ControlClick($winoverride,"","WindowsForms10.COMBOBOX.app.0.378734a1") ;set seconds in dropbox
		 Send("S") ;S for seconds.....
		 Send("{ENTER}")
		 sleep(500) ;wait for seconds to settle
		 ControlClick($winoverride,"","WindowsForms10.BUTTON.app.0.378734a2") ;click OK
		 $winid = WinWaitActive("Relay Timer R2X")
		 ControlClick($winid,"","WindowsForms10.Window.8.app.0.378734a25")
   ElseIf $md = 1;
		 ;do some other stuff, like ask for options, or return to already running application, do other stuff with it, etc
   EndIf

	Return $winid  ;return handle of the RelayTimerR2X
EndFunc

Func MaxDl( $md, $arg)
   If $md = 0 Then
	  Local $MyPid = Run("C:\Program Files (x86)\Diffraction Limited\MaxIm DL V6\MaxIm_DL.exe")
	  ;Deal with nagware
	  $nag=WinWaitActive("Time-Limited License","",10)
	  if $nag <> 0 Then
		 ControlClick($nag,"","Button1")
	  EndIf

	  Local $winid = WinWaitActive("MaxIm DL Pro")
	  Send("^t")

	  Local $obs = WinWaitActive("Observatory")
	  ControlClick($obs,"","SysTabControl321")
	  Send("{RIGHT 8}")

	  ControlClick($obs,"","Button82")
	  Send("{DOWN}")
	  Sleep(500)
	  Send("{DOWN}")
	  Sleep(500)
	  Send("{DOWN}")
	  Sleep(500)
	  Send("{ENTER}")
	  Sleep(500)
	  ControlClick($obs,"","SysTabControl321")
	  Send("{LEFT}")
	  Sleep(500)
	  Send("{LEFT}")
	  Sleep(500)
	  Send("{LEFT}")
	  Sleep(500)
	  Send("{LEFT}")
	  Send("!{F4}")

	  Local $winid = WinWaitActive("MaxIm DL Pro")
	  Send("{ALT down}")
	  Send("f")
	  Send("t")
	  Send("{ALT up}")

	  Local $settings = WinWaitActive("Settings")
	  Sleep(2000)
	  Send("!{F4}")


   Else
	  ; do other stuff
   EndIf


Return $MyPid

EndFunc
