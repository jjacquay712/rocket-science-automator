require 'sinatra/base'
require 'sinatra-websocket'
require 'uia'
require 'json'
require 'logger'
require 'auto_click'

# Fix exception being thrown on server close with monkey patch!
module Sinatra
	class Base
		class << self
			def quit!
				begin
					return unless running?

					# Use Thin's hard #stop! if available, otherwise just #stop.
					running_server.respond_to?(:stop!) ? running_server.stop! : running_server.stop
					$stderr.puts "== Sinatra has ended his set (crowd applauds)" unless handler_name =~/cgi/i
					set :running_server, nil
					set :handler_name, nil
				rescue Exception => e  
				end
			end
		end
	end
end

class MultiIO
	def initialize(*targets)
		@targets = targets
	end

	def write(*args)
		@targets.each {|t| t.write(*args)}
	end

	def close
		@targets.each(&:close)
	end
end


class RocketScienceAutomator < Sinatra::Base
	
	set :server, 'thin'
	set :socket, nil

	get '/' do
		erb :application
	end

	# WebSocket end point
	get '/api/events' do
		if request.websocket?
			request.websocket do |ws|
				ws.onopen do
					settings.socket = ws
				end

				ws.onmessage do |msg|
					#EM.next_tick { settings.sockets.each{|s| s.send(msg) } }
				end

				ws.onclose do
					warn "websocket closed"
				end
			end
		end
	end

	# Handles shutdown request
	get('/api/shutdown') {''}
	after '/api/shutdown' do
		#client_log 'Shutting down'
		2.times { Process.kill("TERM", Process.pid) }
	end

	def start_relay_timer
		client_log 'Checking to see if Relay Timer is running'

		# Get reference to application
		relay_timer = Uia.find_element(id: 'SkinForm')

		# Was the window found?
		if relay_timer.nil?
			
			client_log 'Relay Timer not found, starting...'

			# Magic method that fixes things
			EM.defer do
				system '"C:\Program Files (x86)\Serial Port Tool\Relay Timer R2X\RelayTimer.exe"'
			end

			client_log 'Skipping trial'

			register_popup = nil

			# Stupid trial...
			loop do
				register_popup = Uia.find_element(id: 'Register')
				sleep 0.01
				break if !register_popup.nil?
			end

			register_popup.as :window

			client_log 'Dismissing trial'

			later_btn = register_popup.find(id: 'btnRegisterLater')

			sleep 5

			later_btn.click

			client_log 'Waiting for application to start...'

			# Loop until the app is found
			loop do
				# Get reference to application
				relay_timer = Uia.find_element(id: 'SkinForm')
				sleep 0.01
				break if !relay_timer.nil?
			end 
			
		end

		relay_timer.as :window
	end

	def start_maxim_dl
		client_log 'Checking to see if MaxIm DL Pro 6 is running'

		# Get reference to application
		maxim = Uia.find_element(name: /MaxIm DL Pro/i)

		# Was the window found?
		if maxim.nil?
			
			client_log 'MaxIm DL Pro 6 not found, starting...'

			# Magic method that fixes things
			EM.defer do
				system '"C:\Program Files (x86)\Diffraction Limited\MaxIm DL V6\MaxIm_DL.exe"'
			end

			client_log 'Waiting for application to start...'

			client_log 'Dismissing trial'

			tll_popup = nil

			# Stupid trial...
			loop do
				tll_popup = Uia.find_element(name: /License/i)
				sleep 0.01
				break if !tll_popup.nil?
			end
			
			tll_popup = tll_popup.as :window

			tll_popup.visual_state = :normal


			tll_popup.find(id: '1').click

			client_log 'Closed box finding main window'

			sleep 1

			# Loop until the app is found
			loop do
				# Get reference to application
				maxim = Uia.find_element(name: /MaxIm DL Pro/i)
				sleep 0.01
				break if !maxim.nil?
			end 
			
		end

		maxim.as :window
	end

	get '/api/routines/0' do
		client_log 'Started Relay Timer R2X - Routine A'
		
		relay_timer = start_relay_timer

		client_log 'Bringing window to front'

		# Make sure it's not minimized
		relay_timer.visual_state = :normal

		client_log 'Setting manual mode'

		manual_btns = relay_timer.find_all(id: 'btnManual')
		manual_btns.each { |btn| btn.click }

		client_log 'Toggling relay'

		# Find a button
		on_offs = relay_timer.find_all(id: 'btnSlip')
		# Click that puppy
		on_offs.each { |btn| btn.click }
		
		''
	end

	get '/api/routines/1' do
		client_log 'Started Relay Timer R2X - Routine B'
		
		relay_timer = start_relay_timer

		client_log 'Bringing window to front'

		# Make sure it's not minimized
		relay_timer.visual_state = :normal

		client_log 'Configuring'

		config_btn = relay_timer.find(id: 'btnConfigure')

		config_btn.click

		# network_radio = relay_timer.find(id: 'rdNetwork')
		# network_radio.click
		
		# ok_btn = relay_timer.find(id: 'btnOK')
		# ok_btn.click
		
		
		''
	end

	get '/api/routines/2' do
		client_log 'MaxIm DL Pro - Routine A'

		# Get reference to application
		maxim = start_maxim_dl

		client_log 'Bringing window to front'

		# Make sure it's not minimized
		maxim.visual_state = :maximized

		sleep(1)

		mouse_move(530, 147)
		left_click

		client_log 'Clicked button'

		key_down('ctrl')        
		key_stroke('w')  
		key_up('ctrl')

		''
	end

	helpers do

		def client_log(msg)
			log_line = DateTime.now.to_s + ' - ' + msg + "\n"

			settings.logger.info msg

			settings.socket.send(JSON.generate({:functionName => 'appendLog', :params => {:routineCID => params[:cid], :log => log_line}}))
		end

	end

	configure do
		log_path = 'logs/' + DateTime.now.strftime('%F') + '/'

		Dir.mkdir 'logs' if !Dir.exist? 'logs'
		Dir.mkdir log_path if !Dir.exist? log_path

		set(:logger, Logger.new(MultiIO.new(STDOUT, File.open(log_path + DateTime.now.strftime('%H-%M-%S-%p') + '.log', "a+"))))

		settings.logger.level = Logger::INFO

		settings.logger.info 'Rocket Science Automator Initialized'
	end

	EM.defer do
		system "start http://localhost:4567/"
	end

	run! if app_file == $0
	
end
