$(function() {
	var counter = 0;
	var routineSelection = $('#routine-run-selection');
	var logContainer = $('#routine-logs');
	var async = $('#async');

	var eventSocket = new WebSocket('ws://' + window.location.host + '/api/events');

	var routines = [
		'Relay Timer R2X - Routine A',
		'Relay Timer R2X - Routine B',
		'MaxIm DL Pro - Routine A'
	];

	for ( var n in routines ) {
		routineSelection.append('<option value="' + n + '">' + routines[n] + '</option>');
	}

	
	$("#routine-run-submit").click(function() {
		var isAsync = (async.val() == '1');
		var selectedRoutines = routineSelection.val();
	
		if ( selectedRoutines != null ) {
			for ( var n in selectedRoutines ) {
				var aLog = $('<div class="routine-log routine-id-' + counter + '"><strong>' + routines[parseInt(selectedRoutines[n])] + '</strong><textarea class="routine-log-box"></textarea></div>');
				logContainer.prepend(aLog);

				$.get('/api/routines/' + selectedRoutines[n] + '?cid=' + counter);

				aLog.slideDown('slow');

				counter++;
			}
		} else {
			alert('Please select at least one routine.');
		}

	});


	$("#shutdown").click(function() {
		if ( confirm('Are you sure you want to shutdown this automation application?') ) {
			$.get('/api/shutdown');
			$('#routine-logs').slideUp('slow', function() {
				$('.routine-run-form').slideUp("slow", function() {
					window.open('', '_self').close();
				});
			});
			
		}
	});

	var events = {
		appendLog: function() {
			var routineLog = $('.routine-id-' + this.routineCID);
			var routineLogBox = $('.routine-log-box', routineLog);
			routineLogBox.val(this.log + routineLogBox.val());
		}
	};

	eventSocket.onopen = function(event) {};

	eventSocket.onmessage = function(event) {
		var msg = JSON.parse(event.data);
		events[msg.functionName].call(msg.params);
	}

	eventSocket.onerror = function(evt) { console.log(evt) };
});
